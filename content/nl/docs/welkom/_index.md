---
title: Welkom
weight: 10
# menu: {main: {weight: 20}}
categories: 
description: >
  Hoe werkt dit platform en hoe kun je bijdragen?  
  Hier vind je de wegwijzer en instructies om bij te dragen 
---

Welkom bij federatief.datastelsel.nl, het platform voor ons allemaal!

Het federatief datastelsel is een open source project dat voor iedereen toegankelijk is om te
gebruiken en te verbeteren. De ontwikkeling hiervan doen we juist graag met elkaar, dus we kijken
uit naar je betrokkenheid.

Je vindt in deze site drie categorieën:

1. **Welkom** (<span class="fa fa-arrow-left"></span> hier ben je nu)
    
    Hieronder vind je documenten die inzicht geven in hoe we hier met elkaar willen samenwerken. In
    [strategie van samenwerken](/docs/welkom/strategie-van-samenwerken/) en de
    [werkomgeving](/docs/welkom/werkomgeving/) hopen we jullie inzicht en duidelijkheid hierin te
    geven.

2. **[Realisatie](/docs/realisatie)**
    
    Alle documenten en besluiten die we maken voor de realisatie en verbetering van het federatief datastelsel

3. **[Community](/docs/community)**
    
    Onze community, waar je ziet welke kanalen wij nu hebben om elkaar online te ontmoeten en waar we aankondigen waar we offline bijeenkomsten gaan organiseren. 
    
    Hier vind je ook de link naar MatterMost (onze chat)! Zien we je daar?
