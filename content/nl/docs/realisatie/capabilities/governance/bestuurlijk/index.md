---
title: Bestuurlijk
weight: 1
date: 2023-10-17
categories: [Capabilities, Governance, Bestuurlijk]
tags: 
description: >
  Governance | Bestuurlijk: Bestuurlijke overeenkomsten en juridische constructen
---

{{< capabilities-diagram selected="bestuurlijk" >}}

**Governance | Beheer**:

Bestuurlijke overeenkomsten en juridische constructen ...
